--[[
    @Author       : GGELUA
    @Date         : 2021-03-22 09:54:11
    @LastEditTime : 2021-04-25 14:35:33
--]]

--==========================================================================================

local PushClient = package.loaded.PushClient or require("HPS/PushClient")
local PullClient = class("PullClient",PushClient)
PullClient._hp       = false
PullClient._接收事件 = false
PullClient._准备事件 = false
PullClient._连接事件 = false
PullClient._发送事件 = false
PullClient._断开事件 = false
PullClient.接收事件  = false
PullClient.准备事件  = false
PullClient.连接事件  = false
PullClient.发送事件  = false
PullClient.断开事件  = false

local hp_buffer = require "ghpsocket.buffer"

function PullClient:PullClient()
	self._hp = require "ghpsocket.pullclient"(self)
	self._sendbuf 	= hp_buffer(14138, 1024 * 1024, 9527)
	self._recbuf 	= hp_buffer(14138, 1024 * 1024, 9527)
	local psd = {77,11,22,33,44,55,66,77,88,99,00,69}
	self._sendbuf:SetCode(psd)
	self._recbuf:SetCode(psd)
end

function PullClient:连接(lpszRemoteAddress,usPort,bAsyncConnect,lpszBindAddress,usLocalPort)--地址，端口，异步
	self._info = {
		ishead 	= true,
		len 	= self._recbuf:GetHeaderLen()
	}
    return self._hp:Start(lpszRemoteAddress,usPort,bAsyncConnect,lpszBindAddress,usLocalPort)
end

function PullClient:发送(...)
	--添加数据
	self._sendbuf:Reset()
	local arg = {...}
	for i,v in ipairs(arg) do
		if type(v) == 'number' then
			self._sendbuf:AddNumber(v)
		else
		    self._sendbuf:AddString(tostring(v))
		end
	end
	self._sendbuf:Finish()

	self._hp:Send(self._sendbuf)
end

function PullClient:取连接ID()
	return self._hp:GetConnectionID()
end

--准备连接通知
function PullClient:OnPrepareConnect(dwConnID,socket)
    if self._准备事件 then
        ggexpcall(self._准备事件,self,dwConnID,socket)
    elseif self.准备事件 then
        ggexpcall(self.准备事件,self,dwConnID,socket)
    end
    return 0
end

function PullClient:OnConnect(dwConnID)
	if self._连接事件 then
        ggexpcall(self._连接事件,self,dwConnID)
    elseif self.连接事件 then
        ggexpcall(self.连接事件,self,dwConnID)
    end
    return 0
end

--已发送数据通知
function PullClient:OnSend(dwConnID,iLength)
    if self._发送事件 then
        ggexpcall(self._发送事件,self,dwConnID,iLength)
    elseif self.发送事件 then
        ggexpcall(self.发送事件,self,dwConnID,iLength)
    end
    return 1
end

function PullClient:OnReceivePull(dwConnID, iLength)
	local required 	= self._info.len
	local remain 	= iLength
	while remain >= required do
		remain = remain -required --剩余数据长度
		local FR_OK = self._hp:Fetch(self._recbuf,required)
		if FR_OK == 0 then
			if self._info.ishead then--是否是包头
				required = self._recbuf:CheckHeader() --获取包体长度
				if required == 0 then --非法数据
				    self:断开()
				    break
				end
			else
				if self.接收事件 then
					ggexpcall(self.接收事件,self, dwConnID, unpack(self._recbuf:GetData()))
				elseif self._接收事件 and self._接收事件 then
				    ggexpcall(self._接收事件, dwConnID, unpack(self._recbuf:GetData()))
				end
				required = self._recbuf:GetHeaderLen()--获取包头长度
			end
			self._info.ishead 	= not self._info.ishead
			self._info.len 		= required
		else
			break
		end
	end
	return 0
end

function PullClient:OnReceivePack(dwConnID, Data)
	print("PACK:",#Data)
end

function PullClient:OnClose(dwConnID,enOperation,iErrorCode)
    if self._断开事件 then
        ggexpcall(self._断开事件 ,self ,dwConnID,enOperation ,iErrorCode)
    elseif self.断开事件 then
        ggexpcall(self.断开事件 ,self ,dwConnID,enOperation ,iErrorCode)
    end
    return 0
end

return PullClient